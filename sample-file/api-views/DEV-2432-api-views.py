"""API views definitions"""

from netbox.api.viewsets import NetBoxModelViewSet
from .. import filtersets, models
from .serializers import MappingSerializer, HttpHeaderSerializer, SamlConfigSerializer, CacheConfigSerializer


class MappingViewSet(NetBoxModelViewSet):
    """Mapping view set class"""

    queryset = models.Mapping.objects.prefetch_related("http_headers", "tags").all()
    serializer_class = MappingSerializer
    filterset_class = filtersets.MappingFilterSet
    http_method_names = ["get", "post", "patch", "delete", "options"]


class HttpHeaderViewSet(NetBoxModelViewSet):
    """HTTP Header view set class"""

    queryset = models.HttpHeader.objects.prefetch_related("mapping", "tags").all()
    serializer_class = HttpHeaderSerializer
    filterset_class = filtersets.HttpHeaderFilterSet
    http_method_names = ["get", "post", "patch", "delete", "options"]


class SamlConfigViewSet(NetBoxModelViewSet):
    """SAML config view set class"""

    queryset = models.SamlConfig.objects.prefetch_related("mapping", "tags").all()
    serializer_class = SamlConfigSerializer
    http_method_names = ["get", "post", "patch", "delete", "options"]


# BLOCK ADDED BEGIN
class CacheConfigViewSet(NetBoxModelViewSet):
    """Cache config view set class"""

    queryset = models.CacheConfig.objects.prefetch_related("mapping", "tags").all()
    serializer_class = CacheConfigSerializer
    filterset_class = filtersets.CacheConfigFilterSet
    http_method_names = ["get", "post", "patch", "delete", "options"]
# BLOCK ADDED END
