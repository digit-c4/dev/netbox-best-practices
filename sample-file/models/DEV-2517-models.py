"""Models definitions"""

from typing import Any
from urllib.parse import urlparse
from django.core.exceptions import ValidationError
from django.conf import settings
from django.db import models
from django.db.models import Model
from django.urls import reverse
from django.core.validators import URLValidator, MaxValueValidator, MinValueValidator
from django.contrib.postgres.fields.array import ArrayField
from netbox.models import NetBoxModel  # pylint: disable=import-error, no-name-in-module
from utilities.choices import ChoiceSet  # pylint: disable=import-error, wrong-import-order

URL_MAX_SIZE = 2000
DEFAULT_SORRY_PAGE = settings.PLUGINS_CONFIG["netbox_rps_plugin"]["default_sorry_page"]


def clean_url(raw_url):
    """Clean an URL"""

    o = urlparse(raw_url)

    credential = ":".join(
        tuple(filter(lambda item: item is not None, (o.username, o.password)))
    )

    hostname = o.hostname if o.port is None else ":".join((o.hostname, str(o.port)))

    return (
        o._replace(netloc=hostname)
        if len(credential) == 0
        else o._replace(netloc="@".join((credential, hostname)))
    ).geturl()


class FilteredURLField(models.URLField):
    """URLField definition class"""

    def clean(self, value: Any, model_instance: Model | None) -> Any:
        """Clean Field value"""

        return clean_url(super().clean(value, model_instance))


class AuthenticationChoices(ChoiceSet):
    """Authentication choices definition class"""

    key = "Mapping.authentication"

    DEFAULT_VALUE = "none"

    CHOICES = [
        ("none", "None", "dark"),
        ("ldap", "Ldap", "blue"),
        ("ecas", "Ecas", "blue"),
    ]


class ApplyToChoices(ChoiceSet):
    """Apply to choices definition class"""

    key = "HttpHeader.applyTo"

    DEFAULT_VALUE = "request"

    CHOICES = [
        ("request", "Request", "blue"),
        ("response", "Response", "red"),
    ]


class ExtraProtocolChoices(ChoiceSet):
    """Protocol choices definition class"""

    key = "Mapping.protocol"

    WEBSOCKET_SECURE = "websocket"

    CHOICES = [
        (WEBSOCKET_SECURE, "Websocket", "blue"),
    ]


class ProxyBufferChoices(ChoiceSet):
    """Proxy Buffer Selection definition class"""

    key = "Mapping.proxyBufferSize"

    DEFAULT_VALUE=4

    CHOICES = [
        (4, "4", "dark"),
        (32, "32", "blue"),
        (64, "64", "blue"),
        (128, "128", "blue"),
    ]


def default_protocol():
    """Return the default protocols"""
    return []


class Mapping(NetBoxModel):
    """Mapping definition class"""

    source = FilteredURLField(
        max_length=URL_MAX_SIZE,
        blank=False,
        verbose_name="Source",
        validators=[URLValidator(schemes=["http", "https"])],
        unique=True,
    )
    target = FilteredURLField(
        max_length=URL_MAX_SIZE,
        blank=False,
        verbose_name="Target",
        validators=[URLValidator(schemes=["http", "https"])],
    )
    authentication = models.CharField(
        max_length=30,
        choices=AuthenticationChoices,
        default=AuthenticationChoices.DEFAULT_VALUE,
        blank=False,
        verbose_name="Auth",
    )
    testingpage = models.URLField(
        max_length=URL_MAX_SIZE,
        blank=True,
        null=True,
        validators=[URLValidator(schemes=["http", "https"])],
    )
    webdav = models.BooleanField(
        default=False,
    )
    Comment = models.CharField(max_length=500, blank=True)
    gzip_proxied = models.BooleanField(default=False)
    keepalive_requests = models.IntegerField(
        default=1000, validators=[MinValueValidator(100), MaxValueValidator(5000)]
    )
    keepalive_timeout = models.IntegerField(
        default=75, validators=[MinValueValidator(1), MaxValueValidator(300)]
    )
    proxy_cache = models.BooleanField(default=False)
    proxy_read_timeout = models.IntegerField(
        default=60, validators=[MinValueValidator(1), MaxValueValidator(300)]
    )
    client_max_body_size = models.IntegerField(
        default=1, validators=[MinValueValidator(1), MaxValueValidator(255)]
    )
    sorry_page = models.URLField(
        max_length=URL_MAX_SIZE,
        blank=False,
        verbose_name="Sorry Page",
        validators=[URLValidator(schemes=["http", "https"])],
        default=DEFAULT_SORRY_PAGE,
    )
    extra_protocols = ArrayField(
        base_field=models.CharField(max_length=32, choices=ExtraProtocolChoices),
        null=False,
        blank=True,
        verbose_name="Extra Protocols",
        default=default_protocol,
    )

    # BLOCK ADDED BEGIN
    proxy_buffer_size = models.IntegerField(
        default=4, validators=[MinValueValidator(4), MaxValueValidator(128)],
        null=False,
        choices=ProxyBufferChoices
    )
    # Info : Will be (x4) proxy_buffer_size
    proxy_buffer = models.IntegerField(
        default=16, validators=[MinValueValidator(16), MaxValueValidator(512)],
        null=False,
    )
    # Info : Will be (x2) proxy_buffer_size
    proxy_busy_buffer = models.IntegerField(
        default=8, validators=[MinValueValidator(8), MaxValueValidator(256)],
        null=False,
    )
    # BLOCK ADDED END

    class Meta:
        """Meta class"""

        ordering = ("source", "target")
        constraints = [
            models.CheckConstraint(
                check=~models.Q(source__exact=models.F("target")),
                name="%(app_label)s_%(class)s_check_target_source_url",
            )
        ]

    def __str__(self):
        return f"{self.source}"

    def get_absolute_url(self):
        """override"""
        return reverse("plugins:netbox_rps_plugin:mapping", args=[self.pk])

    def clean(self):
        """Clean model method for validation"""
        super().clean()

        if self.source == self.target:
            raise ValidationError(
                {"target": "Target URL cannot be equal to source URL."}
            )


class SamlConfig(NetBoxModel):
    """SAML config definition class"""

    acs_url = models.CharField(
        max_length=URL_MAX_SIZE,
        blank=False,
        verbose_name="ACS URL",
        validators=[URLValidator(message="It must be a url")],
    )
    logout_url = models.CharField(
        max_length=URL_MAX_SIZE,
        blank=False,
        verbose_name="Logout URL",
        validators=[URLValidator(message="It must be a url")],
    )
    force_nauth = models.BooleanField(
        default=False,
    )
    Mapping = models.OneToOneField(
        Mapping,
        on_delete=models.CASCADE,
        related_name="saml_config",
        db_column="mapping_id",
        name="mapping",
    )


class HttpHeader(NetBoxModel):
    """HTTP Header definition class"""

    mapping = models.ForeignKey(
        Mapping, on_delete=models.CASCADE, related_name="http_headers"
    )
    name = models.CharField(max_length=120, blank=False, verbose_name="Header name")
    value = models.CharField(
        max_length=256, null=True, blank=True, verbose_name="Header value"
    )
    apply_to = models.CharField(
        max_length=30,
        choices=ApplyToChoices,
        default=ApplyToChoices.DEFAULT_VALUE,
        blank=False,
        verbose_name="Apply to",
    )

    class Meta:
        """Meta class"""

        unique_together = ["mapping", "name", "apply_to"]
        ordering = ["name"]

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        """override"""
        return reverse("plugins:netbox_rps_plugin:httpheader", args=[self.pk])
