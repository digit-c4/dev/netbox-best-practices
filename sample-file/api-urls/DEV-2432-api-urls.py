"""API URLs definition"""

from netbox.api.routers import NetBoxRouter
from . import views

APP_NAME = 'netbox_rps_plugin'

router = NetBoxRouter()
router.register('mapping', views.MappingViewSet)
router.register('http_header', views.HttpHeaderViewSet)
router.register('saml_config', views.SamlConfigViewSet)
router.register('cache_config', views.CacheConfigViewSet) ## LINE ADDED

urlpatterns = router.urls
