"""API Serializer definitions"""

from rest_framework import serializers
from netbox.api.serializers import NetBoxModelSerializer, WritableNestedSerializer  # pylint: disable=import-error
from ..models import Mapping, HttpHeader, SamlConfig, clean_url


class NestedMappingSerializer(WritableNestedSerializer):
    """Nested Mapping Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:mapping-detail"
    )

    class Meta:
        """Meta Class"""

        model = Mapping
        fields = ("id", "url", "display")


class NestedSamlConfigSerializer(WritableNestedSerializer):
    """Nested SAML Config Serializer class"""

    url = url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:samlconfig-detail"
    )

    class Meta:
        """Meta Class"""

        model = SamlConfig
        fields = (
            "id",
            "url",
            "acs_url",
            "logout_url",
            "force_nauth",
        )


class SamlConfigSerializer(NetBoxModelSerializer):
    """SAML Config Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:samlconfig-detail"
    )

    mapping = NestedMappingSerializer()

    class Meta:
        """Meta Class"""

        model = SamlConfig
        fields = (
            "id",
            "url",
            "acs_url",
            "logout_url",
            "force_nauth",
            "mapping",
            "custom_fields",
            "created",
            "last_updated",
            "tags",
        )


class HttpHeaderSerializer(NetBoxModelSerializer):
    """HTTP Header Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:httpheader-detail"
    )

    mapping = NestedMappingSerializer()

    class Meta:
        """Meta Class"""

        model = HttpHeader
        fields = (
            "id",
            "url",
            "name",
            "value",
            "apply_to",
            "mapping",
            "custom_fields",
            "created",
            "last_updated",
            "tags",
        )


class NestedHttpHeaderSerializer(WritableNestedSerializer):
    """Nested HTTP Header Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:httpheader-detail"
    )

    class Meta:
        """Meta Class"""

        model = HttpHeader
        fields = ("id", "url", "name", "value", "apply_to")


class MappingSerializer(NetBoxModelSerializer):
    """Mapping Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_rps_plugin-api:mapping-detail"
    )
    http_headers = NestedHttpHeaderSerializer(many=True, required=False)
    saml_config = NestedSamlConfigSerializer(required=False, allow_null=True)

    class Meta:
        """Meta Class"""

        model = Mapping
        fields = (
            "id",
            "url",
            "source",
            "target",
            "authentication",
            "testingpage",
            "webdav",
            "Comment",
            "gzip_proxied",
            "keepalive_requests",
            "keepalive_timeout",
            "proxy_cache",
            "proxy_read_timeout",
            "client_max_body_size",
            "extra_protocols",
            "sorry_page",
            "custom_fields",
            "created",
            "last_updated",
            "tags",
            "http_headers",
            "saml_config",
            # BLOCK ADDED BEGIN
            "proxy_buffer_size",
            "proxy_buffer",
            "proxy_busy_buffer",
            # BLOCK ADDED END
        )

    def create(self, validated_data):
        """Be sure that URL is cleaned"""

        source = validated_data.pop("source", None)
        target = validated_data.pop("target", None)

        if source is not None:
            validated_data["source"] = clean_url(source)

        if target is not None:
            validated_data["target"] = clean_url(target)

        return super().create(validated_data)

    def update(self, instance, validated_data):
        """Be sure that URL is cleaned"""

        validated_data["source"] = clean_url(validated_data["source"])
        validated_data["target"] = clean_url(validated_data["target"])

        return super().update(instance, validated_data)
