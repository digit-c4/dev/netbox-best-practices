from rest_framework import serializers
from netbox.api.serializers import NetBoxModelSerializer, WritableNestedSerializer
from ..models import Certificate

class NestedCertificateSerializer(WritableNestedSerializer):
    """Nested Certificate Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_cert_plugin-api:certificate-detail"
    )

    class Meta:
        """Meta class"""

        model = Certificate
        fields = (
            "id",
            "url",
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "cert_created_at",
            "cert_expired_at",
            "state"
        )


class CertificateSerializer(NetBoxModelSerializer):
    """Certificate Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_cert_plugin-api:certificate-detail"
    )

    class Meta:
        """Meta class"""

        model = Certificate
        fields = (
            "id",
            "url",
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "cert_created_at",
            "cert_expired_at",
            "state",
            "custom_fields",
            "created",
            "last_updated",
            "tags",
        )

# BLOCK ADDED BEGIN
    def validate_state(self, value):
        """
        Validate the 'state' field.
        """
        # Define a list of valid state values
        valid_states = ['To be created', 'To be renewed']  # Add more valid states as needed

        # Check if the provided value is in the list of valid states
        if value not in valid_states:
            print("Invalid state value. Must be one of:", valid_states)
            raise serializers.ValidationError("Invalid state value. Must be one of: {}".format(valid_states))

        return value
# BLOCK ADDED END
