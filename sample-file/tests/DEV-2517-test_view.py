"""Mapping Views Test Case"""

from utilities.testing import ViewTestCases  # pylint: disable=import-error
from netbox_rps_plugin.tests.base import BaseModelViewTestCase
from netbox_rps_plugin.models import Mapping


class MappingViewsTestCase(
    BaseModelViewTestCase,
    ViewTestCases.GetObjectViewTestCase,
    ViewTestCases.GetObjectChangelogViewTestCase,
    ViewTestCases.CreateObjectViewTestCase,
    ViewTestCases.EditObjectViewTestCase,
    ViewTestCases.DeleteObjectViewTestCase,
    ViewTestCases.ListObjectsViewTestCase,
    ViewTestCases.BulkImportObjectsViewTestCase,
    ViewTestCases.BulkDeleteObjectsViewTestCase,

):

    """Mapping Views Test Case Class"""

    model = Mapping

    @classmethod
    def setUpTestData(cls):  # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""
        mapping1 = Mapping.objects.create(
            source="https://truc00.com/api", target="http://10.10.10.11:1800/api"
        )
        mapping2 = Mapping.objects.create(
            source="https://truc01.com/api", target="http://10.10.10.12:1800/api"
        )
        mapping3 = Mapping.objects.create(
            source="https://truc02.com/api", target="http://10.10.10.13:1800/api"
        )

        cls.form_data = {
            "source": "https://truc05.com/api",
            "target": "http://10.10.10.15:1800/api",
            "authentication": "ldap",
            "keepalive_requests": 600,
            "keepalive_timeout": 10,
            "proxy_read_timeout": 60,
            "client_max_body_size": 60,
            "sorry_page": "http://10.10.10.15:1800/sorry_page",
            "proxy_buffer_size": 32, # LINE ADDED HERE
        }

        cls.csv_update_data = (
            "id,source,target,keepalive_requests,keepalive_timeout,"
                "proxy_read_timeout,client_max_body_size,sorry_page",
            f"{mapping1.pk},https://truc07.com/api,http://10.10.10.11:1800/new_api,\
                600,10,60,60,https://10.10.10.15:1800/sorry_page",
            f"{mapping2.pk},https://truc08.com/api,http://10.10.10.12:1800/new_api,\
                600,10,60,60,https://10.10.10.15:1800/sorry_page",
            f"{mapping3.pk},https://truc09.com/api,http://10.10.10.13:1800/new_api,\
                600,10,60,60,https://10.10.10.15:1800/sorry_page",
        )

        cls.csv_data = (
            "source,target",
            "https://truc04.com/api,https://truc039.com/api",
            "https://truc05.com/api,https://truc069.com/api",
            "https://truc06.com/api,https://truc049.com/api",
        )
