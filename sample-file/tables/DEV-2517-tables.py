"""Tables definitions"""

import django_tables2 as tables
from netbox.tables import NetBoxTable, ChoiceFieldColumn, columns  # pylint: disable=import-error, no-name-in-module
from .models import Mapping, HttpHeader


class MappingTable(NetBoxTable):
    """Mapping Table definition class"""

    authentication = ChoiceFieldColumn()
    source = tables.Column(linkify=True)
    httpheader_count = columns.LinkedCountColumn(
        viewname="plugins:netbox_rps_plugin:httpheader_list",
        url_params={"mapping": "pk"},
        verbose_name="HTTP Headers count",
    )
    tags = columns.TagColumn()

    class Meta(NetBoxTable.Meta):
        """Meta class"""

        model = Mapping
        fields = (
            "pk",
            "id",
            "source",
            "target",
            "authentication",
            "testingpage",
            "webdav",
            "extra_protocols",
            "Comment",
            "gzip_proxied",
            "keepalive_requests",
            "keepalive_timeout",
            "proxy_cache",
            "proxy_read_timeout",
            "client_max_body_size",
            "httpheader_count",
            "sorry_page",
            "tags",
            "created",
            "last_updated",
            # BLOCK ADDED BEGIN
            "proxy_buffer_size",
            "proxy_buffer",
            "proxy_busy_buffer",
            # BLOCK ADDED END
        )
        default_columns = (
            "source",
            "target",
            "authentication",
            "webdav",
            "extra_protocols",
            "gzip_proxied",
            "keepalive_requests",
            "keepalive_timeout",
            "proxy_cache",
            "proxy_read_timeout",
            "client_max_body_size",
            "httpheader_count",
        )


class HttpHeaderTable(NetBoxTable):
    """HTTP header Table definition class"""

    name = tables.Column(linkify=True)

    class Meta(NetBoxTable.Meta):
        """Meta class"""

        model = HttpHeader
        fields = (
            "pk",
            "id",
            "mapping",
            "name",
            "value",
            "apply_to",
            "tags",
            "created",
            "last_updated",
        )
        default_columns = ("mapping", "name", "value", "apply_to")
