"""Forms definitions"""

from django import forms
from netbox.forms import (
    NetBoxModelForm,
    NetBoxModelImportForm,
    NetBoxModelFilterSetForm,
)
from utilities.forms.widgets import DatePicker
from utilities.forms.fields import TagFilterField
from .models import Certificate, ExpirationTimeChoices, CaChoices, StateChoices
from django.core.exceptions import ValidationError


class CertificateAddForm(NetBoxModelForm):
    """Certificate form definition class"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['state'].widget.attrs['readonly'] = True
        if 'state' in self.initial and self.initial['state'] != 'To be created':
            raise ValidationError("Initial state must be 'To be created'", code=400)


    class Meta:
        """Meta class"""

        model = Certificate
        fields = (
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "state",
            "cert_created_at",
            "cert_expired_at",
            "tags",
        )
        help_texts = {"CN": "Unique Common Name", "CA": "Valid Certificate Authority"}
        labels = {
            "cn": "CN",
            "alt_name": "Alt name",
            "ca": "CA",
            "expiration_time": "Expiration time",
            "state": "Certificate state",
            "cert_created_at": "Effective certificate's creation date",
            "cert_expired_at": "Effective certificate's expiration date",
        }
        widgets = {
            "cert_created_at": DatePicker(),
            "cert_expired_at": DatePicker(),
        }


class CertificateEditForm(NetBoxModelForm):
    """Certificate form definition class"""

    # BLOCK ADDED BEGIN
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Set initial value for the state field
        if self.instance:
            # Set initial value only if the form is bound to an instance
            self.initial['state'] = 'To be renewed'
            self.fields['state'].widget.attrs['readonly'] = True
        else:
            # If the form is not bound to an instance (e.g., when adding a new certificate),
            # set the initial value to None
            self.initial['state'] = None
    # BLOCK ADDED END

    class Meta:
        """Meta class"""

        model = Certificate
        fields = (
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "state",
            "cert_created_at",
            "cert_expired_at",
            "tags",
        )
        help_texts = {"CN": "Unique Common Name", "CA": "Valid Certificate Authority"}
        labels = {
            "cn": "CN",
            "alt_name": "Alt name",
            "ca": "CA",
            "expiration_time": "Expiration time",
            "state": "Certificate state",
            "cert_created_at": "Effective certificate's creation date",
            "cert_expired_at": "Effective certificate's expiration date",
        }
        widgets = {
            "cert_created_at": DatePicker(),
            "cert_expired_at": DatePicker(),
        }


class CertificateFilterForm(NetBoxModelFilterSetForm):
    """Certificate filter form definition class"""

    model = Certificate
    cn = forms.CharField(
        label="Common Name", max_length=256, min_length=1, required=False
    )
    alt_name = forms.CharField(
        label="Alt name", max_length=256, min_length=1, required=False
    )
    ca = forms.MultipleChoiceField(
        label="Certificate Authority", choices=CaChoices, required=False
    )
    expiration_time = forms.MultipleChoiceField(
        label="Expiration time", choices=ExpirationTimeChoices, required=False
    )
    state = forms.CharField(
        label="Certificate state", required=False, widget=forms.TextInput
    )
    cert_created_at = forms.DateField(
        label="Effective certificate's creation date", required=False, widget=DatePicker
    )
    cert_expired_at = forms.DateField(
        label="Effective certificate's expirations date",
        required=False,
        widget=DatePicker,
    )
    tag = TagFilterField(model)


class CertificateImportForm(NetBoxModelImportForm):
    """Certificate importation form definition class"""


    class Meta:
        """Meta class"""

        model = Certificate
        fields = (
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "state",
            "cert_created_at",
            "cert_expired_at",
        )
        labels = {
            "ca": "Certificate Authority. Can be 'letsencrypt', 'comisign', 'globalsign'",
            "expiration_time": "Expiration time needed. Can be '1m', '3m', '6m', '1y', '3y'",
            "state": "Certificate status. Can be 'pending', 'valid', 'to_be_renewed'",
            "alt_name": "Alt name separated by commas, encased with double quotes"
            + '(e.g. "alt1,alt2,alt3")',
        }

