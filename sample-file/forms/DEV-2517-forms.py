"""Forms definitions"""

from django import forms
from django.utils.translation import gettext as _
from netbox.forms import (  # pylint: disable=import-error, no-name-in-module
    NetBoxModelForm,  # pylint: disable=import-error, no-name-in-module
    NetBoxModelFilterSetForm,  # pylint: disable=import-error, no-name-in-module
    NetBoxModelImportForm,  # pylint: disable=import-error, no-name-in-module
)   # pylint: disable=import-error, no-name-in-module
from utilities.forms.fields import DynamicModelMultipleChoiceField, TagFilterField  # pylint: disable=import-error, wrong-import-order
from utilities.forms import BOOLEAN_WITH_BLANK_CHOICES, add_blank_choice  # pylint: disable=import-error, wrong-import-order
from .models import (
    Mapping,
    AuthenticationChoices,
    ExtraProtocolChoices,
    ProxyBufferChoices,
    HttpHeader,
    ApplyToChoices,
    SamlConfig,
    URL_MAX_SIZE,
)


class MappingForm(NetBoxModelForm):
    """Mapping form definition class"""

    extra_protocols = forms.MultipleChoiceField(
        choices=ExtraProtocolChoices, required=False
    )

    class Meta:
        """Meta class"""

        model = Mapping
        fields = (
            "source",
            "target",
            "authentication",
            "webdav",
            "extra_protocols",
            "testingpage",
            "gzip_proxied",
            "keepalive_requests",
            "keepalive_timeout",
            "proxy_cache",
            "proxy_read_timeout",
            "client_max_body_size",
            "sorry_page",
            "Comment",
            "tags",
            "proxy_buffer_size", # LINE ADDED
        )
        help_texts = {
            "target": "URL-friendly unique shorthand",
            "keepalive_requests": "Min value 100 requests, max 5000 requests, "
            + "default 1000 requests.",
            "keepalive_timeout": "In seconds. Min value 1, max 300, default 75.",
            "proxy_read_timeout": "In seconds. Min value 1, max 300, default 60.",
            "client_max_body_size": "In Mega Bytes. Min 1, max 255, default 1.",
            "proxy_buffer_size": "In Kilo Bytes, min value 4, max value 128 - possible values: 4 kB, 32 kB, 64 kB, 128 kB", # LINE ADDED
        }
        labels = {
            "source": "Source URL",
            "target": "Target URL",
            "testingpage": "Testing Page URL",
            "keepalive_timeout": "Keepalive timeout (s)",
            "proxy_read_timeout": " Proxy read timeout (s)",
            "client_max_body_size": "Client max body size (MB)",
            "sorry_page": "Sorry Page URL",
            "protocols": "Protocols",
            "proxy_buffer_size": "Proxy Buffer Size", # LINE ADDED
        }


class MappingFilterForm(NetBoxModelFilterSetForm):
    """Mapping filter form definition class"""

    model = Mapping
    source = forms.CharField(
        max_length=URL_MAX_SIZE, min_length=1, required=False, label="Source URL"
    )
    target = forms.CharField(
        max_length=URL_MAX_SIZE, min_length=1, required=False, label="Target URL"
    )
    authentication = forms.MultipleChoiceField(
        choices=AuthenticationChoices,
        required=False,
    )
    webdav = forms.BooleanField(
        required=False, widget=forms.Select(choices=BOOLEAN_WITH_BLANK_CHOICES)
    )
    extra_protocols = forms.MultipleChoiceField(
        choices=ExtraProtocolChoices,
        required=False,
    )
    testingpage = forms.CharField(
        max_length=120, min_length=1, required=False, label="Testing URL"
    )
    Comment = forms.CharField(
        max_length=500, min_length=1, required=False, label="Comment"
    )
    gzip_proxied = webdav = forms.BooleanField(
        required=False, widget=forms.Select(choices=BOOLEAN_WITH_BLANK_CHOICES)
    )
    keepalive_requests = forms.IntegerField(
        min_value=100, max_value=5000, required=False
    )
    keepalive_timeout = forms.IntegerField(min_value=1, max_value=300, required=False)
    proxy_cache = forms.BooleanField(
        required=False, widget=forms.Select(choices=BOOLEAN_WITH_BLANK_CHOICES)
    )
    proxy_read_timeout = forms.IntegerField(min_value=1, max_value=300, required=False)
    client_max_body_size = forms.IntegerField(
        min_value=1, max_value=255, required=False
    )
    sorry_page = forms.CharField(
        max_length=URL_MAX_SIZE, min_length=1, required=False, label="Sorry Page URL"
    )
    # BLOCK ADDED BEGIN
    proxy_buffer_size = forms.MultipleChoiceField(
        choices=ProxyBufferChoices,
        required=True,
    )
    # BLOCK ADDED END
    tag = TagFilterField(model)


class MappingImportForm(NetBoxModelImportForm):
    """Mapping importation form definition class"""

    authentication = forms.MultipleChoiceField(
        choices=AuthenticationChoices,
        required=False,
        help_text='Authentication method. Can be "none", "ldap" and "ecas". Default to "none".',
    )
    keepalive_requests = forms.IntegerField(
        min_value=100,
        max_value=5000,
        required=False,
        help_text="Min value 100 requests, max 5000 requests, default 1000 requests.",
    )
    keepalive_timeout = forms.IntegerField(
        min_value=1,
        max_value=300,
        required=False,
        help_text="In seconds. Min value 1, max 300, default 75.",
    )
    proxy_read_timeout = forms.IntegerField(
        min_value=1,
        max_value=300,
        required=False,
        help_text="In seconds. Min value 1, max 300, default 60.",
    )
    client_max_body_size = forms.IntegerField(
        min_value=1,
        max_value=255,
        required=False,
        help_text="In Mega Bytes. Min 1, max 255, default 1.",
    )
    sorry_page = forms.CharField(
        max_length=URL_MAX_SIZE,
        min_length=1,
        required=False,
        help_text="Sorry Page URL",
    )

    class Meta:
        """Meta class"""

        model = Mapping
        fields = (
            "source",
            "target",
            "authentication",
            "testingpage",
            "webdav",
            "testingpage",
            "Comment",
            "gzip_proxied",
            "keepalive_requests",
            "keepalive_timeout",
            "proxy_cache",
            "proxy_read_timeout",
            "client_max_body_size",
            "sorry_page",
        )
        help_texts = {
            "source": "Source URL",
            "target": "Target URL",
            "testingpage": "Testing page URL",
            "webdav": 'Define if the mapping handle Webdav protocol. Default to "false"',
            "gzip_proxied": 'Is gzip proxied. Default to "false"',
            "proxy_cache": 'Is proxy cache activated. Default to "false"',
        }


class HttpHeaderForm(NetBoxModelForm):
    """HTTP header form definition class"""

    class Meta:
        """Meta class"""

        model = HttpHeader
        fields = ("mapping", "name", "value", "apply_to")
        labels = {
            "mapping": "Mapping",
            "name": "Name",
            "value": "Value",
            "apply_to": "Apply to",
        }


class HttpHeaderFilterForm(NetBoxModelFilterSetForm):
    """HTTP header filter form definition class"""

    model = HttpHeader
    name = forms.CharField(
        max_length=120, min_length=1, required=False, label="Header name"
    )
    value = forms.CharField(
        max_length=256, min_length=1, required=False, label="Header value"
    )
    apply_to = forms.ChoiceField(
        choices=add_blank_choice(ApplyToChoices), required=False, label="Apply to"
    )
    mapping = DynamicModelMultipleChoiceField(
        queryset=Mapping.objects.all(), required=False, label=_("Mapping")
    )
    tag = TagFilterField(model)


class SamlConfigForm(NetBoxModelForm):
    """SAML config form definition class"""

    class Meta:
        """Meta class"""

        model = SamlConfig
        fields = ("mapping", "acs_url", "logout_url", "force_nauth")
        labels = {
            "mapping": "Mapping",
            "acs_url": "ACS URL",
            "logout_url": "Logout URL",
            "force_nauth": "Force AuthnRequest",
        }
