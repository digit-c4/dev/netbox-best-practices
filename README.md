# Netbox Best Practices
## Introduction

In this repository, we will gather every good pratice and standard trick that ease the development of netbox feature.
The proposed format is currently divised by "Exercice" in the form of real example fulfill for the CMDB SQUAD.

## Exercice 0 <WORK IN PROGRESS>: Setup of netbox cmdb and plugins.

For each CMDB user story, we have at least to setup a netbox cmdb (from original lib) and a related plugin.
The way it is organized is still WORK IN PROGRESS.

But, if you have any question on it, you can contact our Architect Raphael JOIE (raphael.joie@ext.ec.europa.eu).

## Exercice 1: Add parameter into an existing model.

If your request only scope addition of parameter of an existing object in netbox plugin, follow this exercice :

The example is what were asked for DEV-2517 leankit card.

### Sample Files

Localisation : ./sample-file/\*/DEV-2517-\*.py

### Modify the model

Add every parameter, based on the table described in your card, if you are unsure of what to add, request more detail to your CMDB point of contact.

### Modify the form

So your model modifications is visible through any "add" or "edit" request in the GUI.

### Modify the table

So your model modifications is visible through the "list" of instances related to your modified object.

### Modify the serializer

So you define how your model should be "save" from every sources (GUI and API).

### The rest

- In an existing model, no modification of the GUI view should be necessary.
- In an existing model, no modification of the API view should be necessary.

### Perform Test

Make a dedicated test for your new parameter.
Refers to Exercice 3 to see how to make your test.

### Check Lint and coverage.


## Exercice 2: Add a whole new model into an existing plugin.

If your request scope the creation a brand new object in an existing netbox plugin, follow this exercice :

The example is what were asked for DEV-2432 leankit card.

### Sample Files

Localisation : ./sample-file/\*/DEV-2432-\*.py

### Modify the model

Add a dedicated python class, in the same section of the existing models.
Add every parameter, based on the table described in your card, if you are unsure of what to add, request more detail to your CMDB point of contact.

### Modify the form

So your model modifications is visible through any "add" or "edit" request in the GUI.

### Modify the table

So your model modifications is visible through the "list" of instances related to your modified object.

### Modify the serializer

So you define how your model should be "save" from every sources (GUI and API).
Warning, the way it is defined will differ either your model is independant from any other model or a complex parameter associate to another instance.

In the example 2432, it is an association.

### Modify the gui view

In a brand new model case, you need to add your model into the gui views so each instance can be "get" in the GUI.

### Modify the api view

In a brand new model case, you need to add your model into the "api/views" so each instance can be "get" from the API.

### Modify the gui url

In a brand new model case, you need to define the GUI url used for each action you want to perform to the model's instance.
Warning, the way it is defined will differ either your model is independant or a complex parameter associate to another instance.

In the example 2432, it is an association.

### Modify the api url

In a brand new model case, you need to define the API url, this should represent a link to the api views.

### Perform Test

Make a dedicated test for your new model, with each defined action (create, update, delete, at minima).
Refers to Exercice 3 to see how to make your test.

### Check Lint and coverage.

## Exercice 3: Create personnalized unit test (GUI/API).

For any test, a personnalized unit test is generally cut into distinct phases :

- Add object-level permission
- Get/Set the original context
- Make the operation to be check
- Compare with the expected outcome

For this exercice, we will use DEV-2432 example.

### Sample File

Localisation : ./sample-file/tests/DEV-2432-cache_config_api.py

### Add object-level permission

In order to your test to be able to mock a user permission so it is actually able perform the operation in a same condition.

### Get/Set the original context

If the data is not defined at the global level, then you have the possibility to setup your own test data before doing the actual action.

### Make the operation to be check

Based on the model and parameter to be checked, you have to define them at this point.
At the sample, you have a good glimpse on how to perform a create, update and delete action.

### Compare with the expected outcome

The actual test is in this section.
You may want to check the actual values you know to expect.
But in a standard test, there is at least a check of the HTTP Status code returned by your previous operation.
- 201 for a successful creation.
- 200 for a successful update.
- 204 for a successful deletion.

## Exercice 4: How to force a field's value in a form.

From DEV-2371, we need to force a parameter of a model based on form triggered by the user.

- If it is a new instance form, then the value of the parameter should be "To be Created".
- If it is an edit instance form, then the value of the parameter should be "To be Renewed".

Based on this request, we were able to write a new pre-task before sending the form.

See sample-file/forms/DEV-2371-forms.py for more information.

## Exercice 5: About "validate_state" in serializer class.

From DEV-2371, we need to check if the parameter of a model have specific value at precise moment.

In order to do so, we had a 'magic' method inside an existing class beloging to the Serializer.

See sample-file/serializers/DEV-2371-serializers.py for more information

## Exercise 6: How to handle multiple selection form field.

From DEV-2482, we need to add a multiple field selection.

In order to do so we modified the file forms.py where our list_extensions has to be present not only in CacheConfigForm as well as in CacheConfigFilterForm in order to have the multiple selection field.

The model, as well has been adapted and a new calss that contains all the possible choices called ExtensionChoices has been created.
