# Introduction

This file is here to details the pre-requisite when you inherite the pre-formatted test to your unittest class.


# GetObjectViewTestCase

## Principle

This ViewTestCase simulate a user requesting details page on one instance into the GUI.

## Setup.

In order to make this code works, you need at least 2 instances in the global "setUpTestData" classmethod.
Just put the required fields in their parameters enumerators.

Example :

```python
    @classmethod
    def setUpTestData(cls):  # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""
        mapping1 = Mapping.objects.create(
            source="https://truc00.com/api", target="http://10.10.10.11:1800/api"
        )
        mapping2 = Mapping.objects.create(
            source="https://truc01.com/api", target="http://10.10.10.12:1800/api"
        )
        mapping3 = Mapping.objects.create(
            source="https://truc02.com/api", target="http://10.10.10.13:1800/api"
        )
```

## Required data in database.

Nothing known

# GetObjectChangelogViewTestCase

## Principle

This ViewTestCase simulate a user requesting details page on the ChangeLog View into the GUI.

## Setup.

Nothing known.

## Required data in database.

Nothing known.

# CreateObjectViewTestCase

## Principle

This ViewTestCase simulate a user action requesting a new instance of the tested object into the GUI.

## Setup.

Self.form_data must be already set in the global "setUpTestData" classmethod.
Just put the required fields as dictionnary key-value pair.

Example :

```python
    @classmethod
    def setUpTestData(cls):  # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""

        cls.form_data = {
            "source": "https://truc05.com/api",
            "target": "http://10.10.10.15:1800/api",
            "authentication": "ldap",
            "keepalive_requests": 600,
            "keepalive_timeout": 10,
            "proxy_read_timeout": 60,
            "client_max_body_size": 512,
            "sorry_page": "http://10.10.10.15:1800/sorry_page",
            "proxy_buffer_size": 32,
        }
```

## Required data in database.

Nothing known

# EditObjectViewTestCase

## Principe

This ViewTestCase simulate a user action requesting a update of instance of the tested object into the GUI.

## Setup.

Self.form_data must be already set in the global "setUpTestData" classmethod.
Just put the required fields as dictionnary key-value pair.

Example :

```python
    @classmethod
    def setUpTestData(cls):  # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""

        cls.form_data = {
            "source": "https://truc05.com/api",
            "target": "http://10.10.10.15:1800/api",
            "authentication": "ldap",
            "keepalive_requests": 600,
            "keepalive_timeout": 10,
            "proxy_read_timeout": 60,
            "client_max_body_size": 512,
            "sorry_page": "http://10.10.10.15:1800/sorry_page",
            "proxy_buffer_size": 32,
        }
```

## Required data in database.

At least one instance with their primary key matching the update request primary key value(s).

# DeleteObjectViewTestCase

## Principle.

This ViewTestCase simulate a user action requesting a suppression of instance of the tested object into the GUI.

## Setup.

Nothing known

## Required data in database.

At least one instance with their primary key matching the delete request primary key value(s).

# ListObjectsViewTestCase

## Principle

This ViewTestCase simulate a user action requesting the list of existing instance of a given object into the GUI.

## Setup.

In order to make this code works, you need at least 2 instances in the global "setUpTestData" classmethod.
Just put the required fields in their parameters enumerators.

Example :

```python
    @classmethod
    def setUpTestData(cls):  # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""
        mapping1 = Mapping.objects.create(
            source="https://truc00.com/api", target="http://10.10.10.11:1800/api"
        )
        mapping2 = Mapping.objects.create(
            source="https://truc01.com/api", target="http://10.10.10.12:1800/api"
        )
        mapping3 = Mapping.objects.create(
            source="https://truc02.com/api", target="http://10.10.10.13:1800/api"
        )
```

## Required data in database.

Nothing known

# BulkImportObjectsViewTestCase

## Principle

This ViewTestCase simulate a user action requesting the import of several data instances through a CSV file into the GUI.

## Setup.

Self.csv_data must be already set in the global "setUpTestData" classmethod.
Just put the required fields as a tuple like in the example.

Example :

```python
    @classmethod
    def setUpTestData(cls):  # pylint: disable=invalid-name
        """Initial Data setup to perform the 'parent' ViewTestCases"""

        cls.csv_data = (
            "source,target",
            "https://truc04.com/api,https://truc039.com/api",
            "https://truc05.com/api,https://truc069.com/api",
            "https://truc06.com/api,https://truc049.com/api",
        )
```

## Required data in database.

Nothing known

# BulkDeleteObjectsViewTestCase

## Principe

This ViewTestCase simulate a user action requesting the deletion of several instances at once through a dedicated button into the GUI.

## Setup.

Nothing known

## Required data in database.

At least three instances with their primary key matching the delete request primary key value(s).